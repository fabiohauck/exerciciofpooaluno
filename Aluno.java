
public class Aluno 
{
    
    private String nome;
    private int nota;
    
    public Aluno(String nomeAluno, int notaAluno)
    {
      this.nome = nomeAluno;
      this.nota = notaAluno;
    }

   boolean estaAprovado()
    {
       return (nota >= 7);
    }
    
    public void setNota (int novaNota)
    {
        if(novaNota >= 0 && novaNota <= 10)
        {
            this.nota = novaNota;   
        }
    }
    
    public int getNota()
    {
        return nota;
    }
    
       public void setNome (String novoNome)
    {
        
            this.nome = novoNome;   
        
    }
    
    public String getNome()
    {
        return nome;
    }
}
