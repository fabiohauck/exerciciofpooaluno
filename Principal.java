
public class Principal 
{
     public static void main(String[] args){
         
         System.out.print("#### SISTEMA");
         System.out.println(" DE NOTAS");
         
         Aluno aluno1, aluno2;
         
         aluno1 = new Aluno("Zezin", 9);
         aluno2 = new Aluno("Pedrinho", 4);
         
         aluno1.setNota(15);
         aluno2.setNota(5);
         
         if(aluno1.estaAprovado())
         {
             System.out.println("Aluno "+ aluno1.getNome() +" esta aprovado " + aluno1.getNota());
         
         }
         else
         {
             System.out.println("Aluno "+ aluno1.getNome() +" está reprovado " + aluno1.getNota());
         
         }
         
         if(aluno2.estaAprovado())
         {
             System.out.println("Aluno "+ aluno2.getNome() +"esta aprovado " + aluno2.getNota());
         
         }
         else
         {
             System.out.println("Aluno "+ aluno2.getNome() +"está reprovado " + aluno2.getNota());
         
         }
         
     }
}
